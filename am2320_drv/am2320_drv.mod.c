#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xaa11f9d6, "module_layout" },
	{ 0x25b1b02e, "device_create" },
	{ 0xedd88526, "__class_create" },
	{ 0x261c850b, "__register_chrdev" },
	{ 0x9e7d6bd0, "__udelay" },
	{ 0x6e2cfa0e, "i2c_transfer" },
	{ 0x6bc3fbc0, "__unregister_chrdev" },
	{ 0xc3174b9e, "class_destroy" },
	{ 0x9c0140d5, "device_destroy" },
	{ 0x27e1a049, "printk" },
	{ 0xde33d1ed, "i2c_register_driver" },
	{ 0xd8f1480a, "i2c_put_adapter" },
	{ 0xcdc29ba0, "i2c_new_probed_device" },
	{ 0x1122e4f2, "i2c_get_adapter" },
	{ 0x2bc95bd4, "memset" },
	{ 0x2c0fdeca, "i2c_unregister_device" },
	{ 0xf9ed3ff4, "i2c_del_driver" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "01F7C1BDBA7732E8020F937");
